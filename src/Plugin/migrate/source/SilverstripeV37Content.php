<?php

namespace Drupal\migrate_silverstripe\Plugin\migrate\source;

/**
 * Migrate content from Silverstripe v3.
 *
 * Extend this plugin with support for custom content
 * features in your Silverstripe source.
 *
 * @MigrateSource(
 *   id = "silverstripe_v3_7_content",
 *   source_module = "migrate_silverstripe",
 * )
 */
class SilverstripeV37Content extends SilverstripeV3Content {

}
