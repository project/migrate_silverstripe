<?php

namespace Drupal\migrate_silverstripe\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Migrate content from Silverstripe v3.
 *
 * Extend this plugin with support for custom content
 * features in your Silverstripe source.
 *
 * @MigrateSource(
 *   id = "silverstripe_v3_content",
 *   source_module = "migrate_silverstripe",
 * )
 */
class SilverstripeV3Content extends SqlBase {

  /**
   * Token replacements and patterns.
   *
   * @var array
   */
  protected $tokenPatterns = [
    'replaceSiteTreeLinkToken' => '/\[sitetree_link,id=([0-9]+)\]/i',
    'replaceFileLinkToken' => '/\[file_link,id=([0-9]+)\]/i',
  ];

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Source data is queried from 'SiteTree_Live' table.
    $query = $this->select('SiteTree_Live', 'stl')
      ->fields('stl', [
        'ID',
        'Title',
        'Content',
        'Created',
        'LastEdited',
        'ParentID',
        'URLSegment',
      ]);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'ID' => $this->t('ID'),
      'Title' => $this->t('Title'),
      'Created' => $this->t('Created'),
      'LastEdited' => $this->t('LastEdited'),
      'Content' => $this->t('Content'),
      'Path' => $this->t('Path'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'ID' => [
        'type' => 'integer',
        'alias' => 'stl',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function prepareRow(Row $row) {
    // Convert Silverstripe's Created and LastEdited ("2016-01-21 11:34:55")
    // to timestamps.
    //
    // @todo no TZ here; account for Silverstripe timezone setting.
    if ($created = $row->getSourceProperty('Created')) {
      $row->setSourceProperty('tsCreated', strtotime($created));
    }
    if ($lastEdited = $row->getSourceProperty('LastEdited')) {
      $row->setSourceProperty('tsLastEdited', strtotime($lastEdited));
    }
    $row->setSourceProperty('Path', $this->getSiteTreePath(
      $row->getSourceProperty('ParentID'),
      $row->getSourceProperty('URLSegment')
    ));
    $row->setSourceProperty('Content', $this->replaceSilverStripeTokens(
      $row->getSourceProperty('Content')
    ));
    return parent::prepareRow($row);
  }

  /**
   * Get token patterns.
   */
  protected function getTokenPatterns() {
    return $this->tokenPatterns;
  }

  /**
   * Replace [sitetree_link,id=123] with a URL.
   *
   * @param string|array $value
   *   Input value for replacement.
   *
   * @return string|string[]|null
   *   Replaced version of input.
   */
  protected function replaceSilverStripeTokens($value) {
    foreach ($this->getTokenPatterns() as $method => $pattern) {
      $value = preg_replace_callback($pattern, [$this, $method], $value);
    }
    return $value;
  }

  /**
   * Replace tokens like [sitetree_link,id=12345] with the path of the link.
   *
   * @param array $matches
   *   Match result.
   *
   * @return string
   *   Replacement value.
   */
  public function replaceSiteTreeLinkToken(array $matches) {
    return $this->getSiteTreePath($matches[1]);
  }

  /**
   * Replace tokens like [file_link,id=12345] with the path of the file.
   *
   * @param array $matches
   *   Match result.
   *
   * @return string
   *   Replacement value.
   */
  public function replaceFileLinkToken(array $matches) {
    return $this->getFilePath($matches[1]);
  }

  /**
   * Build a path using SilverStripe's paths.
   *
   * @param string|int $id
   *   SilverStripe SiteTree content ID.
   * @param string $current
   *   Optional path component of this content item.
   *
   * @return string
   *   Path per SilverStripe SiteTree.
   */
  public function getSiteTreePath($id, $current = NULL) {
    $urlParts = is_null($current) ? [] : [$current];
    while ($details = $this->getSiteTreeUrlDetails($id)) {
      $id = (empty($details['ParentID'])) ? NULL : $details['ParentID'];
      array_unshift($urlParts, $details['URLSegment']);
    }
    if (!empty($urlParts)) {
      return '/' . implode('/', $urlParts);
    }
    return FALSE;
  }

  /**
   * Get details of a path component from DB.
   *
   * @param int $id
   *   ID of content item in SiteTree_Live table.
   *
   * @return array|bool
   *   Content details if available.
   */
  public function getSiteTreeUrlDetails($id) {
    $query = $this->select('SiteTree_Live', 'stl')
      ->fields('stl', [
        'ID',
        'ParentID',
        'URLSegment',
      ])
      ->condition('ID', $id);
    if ($result = $query->execute()->fetchAssoc()) {
      return $result;
    }
    return FALSE;
  }

  /**
   * Get file path (URL) for the given file ID.
   *
   * @param int $id
   *
   * @return string
   *   File path.
   */
  public function getFilePath($id) {
    $filePath = $this->select('File', 'f')
      ->fields('f', [
        'Filename',
      ])
      ->condition('ID', $id)
      ->execute()
      ->fetchField();

    return $filePath ? '/' . $filePath: "/file-id-$id-not-found" ;
  }

}
