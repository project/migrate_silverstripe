# Migrate Silverstripe

This Drupal Migrate source plugin provides source for migrations from Silverstripe to Drupal.

This migration source queries the source Silverstripe database directly, so the source plugin needs to be capable of comprehending Silverstripe's storage methodology.

There are many versions of Silverstripe so this module is likely to be an ongoing work in progress. You can help!

#### Contributing

The first release of this plugin implements support for Silverstripe v3.7. The intent is that it's structured well enough that adding support for additional versions of Silverstripe, and even popular Silverstripe plugins, will be reasonably easy to contribute into the base module. Alongside this module we encourage you to implement a custom module which adds functionality specific to your site.

#### Collaboration

- https://www.drupal.org/sandbox/grobot/3186781 is our workplace for code, issues, MRs/patches, and (soon) CI.
- Ask in [#migration](https://drupal.slack.com/archives/C226VLXBP) on Drupal Slack.

#### TODO

* Support more Silverstripe versions
* Support content revisions
